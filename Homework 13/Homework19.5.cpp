#include <iostream>
#include <string.h>
using namespace std;

class Animal
{
public:
	string voice;
	Animal()
	{
		voice = "Hello";
	}

	Animal(string _voice)
	{
		voice = _voice;
	}

	void Voice()
	{
		cout << voice << endl;
	}
};

class Dog : public Animal
{
public:
	Dog()
	{
		voice = "Woof!";
	}
};

class Cat : public Animal
{
public:
	Cat()
	{
		voice = "Meow!";
	}
};

class Pig : public Animal
{
public:
	Pig()
	{
		voice = "Oink!";
	}
};

void main()
{
	Animal* AnimalArray[3] = { new Dog, new Cat, new Pig };

	for (int i = 0; i < 3; i++)
	{
		AnimalArray[i]->Voice();
	}
}